import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import Clock from './components/Clock';
import ClickCounter from './components/ClickCounter';
import Toggle from './components/Toggle';
import LoginControl from './components/LoginControl';
import Joke from './components/Joke';
import NamePrompt from './components/NamePrompt';
import Game from './components/tick-tack-toe/Game';
import Cat from './components/Cat';

ReactDOM.render(
  <React.StrictMode>
    <NamePrompt />
    <LoginControl />
    <Clock />
    <ClickCounter />
    <Toggle />
    <Joke />
    <Cat />
    <Game />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
