import React from 'react'

interface LogoutButtonProps {
    handleClick(): void;
}

const LogoutButton: React.FC <LogoutButtonProps> = ({handleClick}) => {
    return (
        <button onClick={handleClick}>
            Wyloguj się
        </button>
    );
}

export default LogoutButton;