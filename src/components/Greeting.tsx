import React from 'react'
import GuestGreeting from './GuestGreeting'
import UserGreeting from './UserGreeting'

interface GreetingProps {
    isLoggedIn: boolean;
}

const Greeting: React.FC<GreetingProps> = ({ isLoggedIn }) => {
    if (isLoggedIn) {
        return <UserGreeting />;
    }
    return <GuestGreeting />;
}

export default Greeting;