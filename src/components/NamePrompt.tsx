import React from 'react'
import Welcome from './Welcome';

export default function NamePrompt() {

    const name = prompt("Czy chciałabyś/łbyś podać swoje imię?");
    return (
        <div>
        <Welcome name={name}/>          
        </div>
    )
}
