import React, { useState } from 'react'
import LoginButton from './LoginButton'
import LogoutButton from './LogoutButton'
import Greeting from './Greeting'


// class LoginControl extends React.Component<any, any> {
//     constructor(props: any) {
//         super(props);
//         this.handleLoginClick = this.handleLoginClick.bind(this);
//         this.handleLogoutClick = this.handleLogoutClick.bind(this);
//         this.state = { isLoggedIn: false };
//     }

//     handleLoginClick() {
//         this.setState({ isLoggedIn: true });
//     }

//     handleLogoutClick() {
//         this.setState({ isLoggedIn: false });
//     }

//     render() {
//         const isLoggedIn = this.state.isLoggedIn;
//         let button;
//         if (isLoggedIn) {
//             button = <LogoutButton onClick={this.handleLogoutClick} />;
//         } else {
//             button = <LoginButton onClick={this.handleLoginClick} />;
//         }

//         return (
//             <div>
//                 <Greeting isLoggedIn={isLoggedIn} />
//                 {button}
//             </div>
//         );
//     }
// }
// export default LoginControl;


const LoginControl: React.FC = () => {
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    const handleClickLogout = () => setIsLoggedIn(false);
    const handleClickLogin = () => setIsLoggedIn(true);

    let button;
    if (isLoggedIn) {
        button = <LogoutButton handleClick={handleClickLogout} />;
    } else {
        button = <LoginButton handleClick={handleClickLogin} />;
    }

    return (
        <div>
            <Greeting isLoggedIn={isLoggedIn} />
            {button}
        </div>
    );
}

export default LoginControl;