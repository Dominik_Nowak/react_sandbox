import React, { useState } from 'react'
import axios from 'axios'


export default function Cat() {
    const [imgUrl, setImgUrl] = useState("https://purr.objects-us-east-1.dream.io/i/hMQqs.gif");

    const getCat = async () => {
        const response = await axios.get("https://aws.random.cat/meow");
        const data = response.data;
        setImgUrl(data.file);
    };

    return (
        <div>
            <img src={imgUrl}
            alt="Randomowe zdjęcie kota" 
            height="350"
            onClick={()=>getCat()}
            ></img>
        </div>
    )
}
