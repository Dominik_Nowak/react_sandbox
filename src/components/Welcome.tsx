import React from 'react';
import { isNull } from 'util';


interface WelcomeProps {
    name?: string | null, //quote means that prop is optional
}

const Welcome: React.FC<WelcomeProps> = ({ name }) => {
    if (isNull(name)) {
        name = "Przybyszu";
    }
    return <h1>Cześć, {name}!</h1>
}


export default Welcome;