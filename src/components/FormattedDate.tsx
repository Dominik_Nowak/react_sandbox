import React from 'react'

interface IProps{
    date: Date
}

const FormattedDate: React.FC<IProps> = ({date}) => {
    return <h2>Aktualny czas: {date.toLocaleTimeString()}.</h2>
}

export default FormattedDate;