import React, { useState } from 'react'

function Toggle() {
    const [isToggleOn, setIsToggleOn] = useState(false);


    return (
        <button onClick={() => setIsToggleOn(!isToggleOn)}>
            {isToggleOn ? 'ON' : 'OFF'}
        </button>
    )
}

export default Toggle;

// class Toggle extends React.Component {
//     constructor(props) {
//       super(props);
//       this.state = {isToggleOn: true};

//       // Poniższe wiązanie jest niezbędne do prawidłowego przekazania `this` przy wywołaniu funkcji
//       this.handleClick = this.handleClick.bind(this);
//     }

//     handleClick() {
//       this.setState(state => ({
//         isToggleOn: !state.isToggleOn
//       }));
//     }

//     render() {
//       return (
//         <button onClick={this.handleClick}>
//           {this.state.isToggleOn ? 'WŁĄCZONY' : 'WYŁĄCZONY'}
//         </button>
//       );
//     }
//   }