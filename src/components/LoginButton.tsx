import React from 'react'

interface IProps {
    handleClick: () => void
}

function LoginButton({ handleClick }: IProps) {
    return (
        <button onClick={handleClick}>
            Zaloguj się
        </button>
    );
}

export default LoginButton;