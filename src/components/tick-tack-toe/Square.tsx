import React from 'react';

type SquareValue = 'X' | 'O' | null;

interface ISquareProps {
  value: SquareValue;
  onClick(): void;
}

const Square: React.FC<ISquareProps> = props => {
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  );
}

export default Square;