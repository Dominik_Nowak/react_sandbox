import React, { useState, useEffect } from 'react';

const ClickCounter: React.FC = () => {
    const [count, setCount] = useState(0);

    useEffect(() => {
        document.title = `Licznik kliknięć: ${count}`;

    }, [count]);

    function showAlert() {
        alert('Naciśnięto przycisk!');
    }

    return (
        <div>
            <p>Naciśnięto {count} razy</p>
            <button onClick={() => { setCount(count + 1); showAlert() }}>
                Naciśnij mnie
            </button>
        </div>
    )
}

export default ClickCounter;