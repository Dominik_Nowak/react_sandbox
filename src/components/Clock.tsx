import React from 'react';
import FormattedDate from './FormattedDate';

interface IProps {
    date?: Date,
}

interface IState {
    date: Date,
    timerID: number;
}

class Clock extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            date: new Date(),
            timerID: 0
        };
    }

    componentDidMount() {
        this.setState({
            timerID: window.setInterval(() => this.tick(), 1000)
        })

    }

    componentWillUnmount() {
        clearInterval(this.state.timerID);
    }

    tick() {
        this.setState({
            date: new Date()
        });
    }

    render() {
        return (
            <div>
                <FormattedDate date={this.state.date} />
            </div>
        );
    }
}

export default Clock;