import React, { useState } from 'react';

export default function Joke() {
    const [setup, setSetup] = useState(null);
    const [punchline, setPunchline] = useState(null);

    const getJoke = async () => {
        const response = await fetch('https://official-joke-api.appspot.com/random_joke');
        const data = await response.json();
        setSetup(data.setup);
        setPunchline(data.punchline);
    }

    return (
        <div>
            <button onClick={getJoke}>Losuj kawał</button>
            <p>{setup}</p>
            <p>{punchline}</p>
        </div>
    );
};
